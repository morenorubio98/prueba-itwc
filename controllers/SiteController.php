<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Empresas;
use app\models\Contactos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $empresas = Empresas::getAllEmpresas()->all();
        $contactos = Contactos::getAllContactos()->all();

        return $this->render('index',compact("empresas","contactos"));
    }

    public function actionGuardar_empresa()
    {
        $guardar_empresa = Empresas::saveEmpresa($_POST);

        return 1;
    }

    public function actionEditar_empresa()
    {
        $empresa = Empresas::getEmpresa($_POST["empresa_id"])->one();

        return json_encode($empresa);
    }

    public function actionModificar_empresa()
    {
        
        $modificar_empresa = Empresas::updateEmpresa($_POST);

        return 1;
    }

    public function actionEliminar_empresa()
    {
    
        $arreglo = ["activo" => 0];
        $eliminar_empresa = Empresas::eliminarEmpresa($arreglo,$_POST["id_empresa"]);

        return 1;
    }

    public function actionGuardar_contacto()
    {
        $guardar_empresa = Contactos::saveContacto($_POST);

        return 1;
    }

    public function actionEditar_contacto()
    {
        $contacto = Contactos::getContacto($_POST["contacto_id"])->one();

        return json_encode($contacto);
    }

    public function actionModificar_contacto()
    {
        
        $modificar_contacto = Contactos::updateContacto($_POST);

        return 1;
    }

    public function actionEliminar_contacto()
    {
    
        $arreglo = ["activo" => 0];
        $eliminar_contacto = Contactos::eliminarContacto($arreglo,$_POST["id_contacto"]);

        return 1;
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
