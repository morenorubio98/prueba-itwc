$( document ).ready(function() {

    $(".frmEmpresa").submit(function(e)
    {
        e.preventDefault();

        var nombre_e = $("#nombre_e").val()
        var direccion_e = $("#direccion_e").val()
        var telefono_e = $("#telefono_e").val()
        var sitio_e = $("#sitio_e").val();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data : {
                nombre_e : nombre_e,
                direccion_e : direccion_e,
                telefono_e : telefono_e,
                sitio_e : sitio_e,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha registrado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
          });
    });

    $(".btnEditE").click(function(){

        var empresa_id = $(this).data("id");
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data : {
                empresa_id : empresa_id,
            },
            dataType: 'json',
            success: function(data){
                console.log(data);
                $("#editEmpresa").modal('show');
                $("#edit_nombre_e").val(data.nombre_e);
                $("#edit_direccion_e").val(data.direccion_e);
                $("#edit_telefono_e").val(data.telefono_e);
                $("#edit_sitio_e").val(data.sitio_e);
                $("#edit_id_empresa").val(data.id_empresa);
            }
          });
    });

    $(".frmEmpresaEdit").submit(function(e)
    {
        e.preventDefault();

        var nombre_e = $("#edit_nombre_e").val()
        var direccion_e = $("#edit_direccion_e").val()
        var telefono_e = $("#edit_telefono_e").val()
        var sitio_e = $("#edit_sitio_e").val();
        var id_empresa = $("#edit_id_empresa").val();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data : {
                nombre_e : nombre_e,
                direccion_e : direccion_e,
                telefono_e : telefono_e,
                sitio_e : sitio_e,
                id_empresa : id_empresa,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha modificado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
          });
    });


    $(".btnEliminarE").click(function(){
        var id_empresa = $(this).data("id");

        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data : {
                id_empresa : id_empresa,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha eliminado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
        });
    });



    //Contacto

    $(".frmContacto").submit(function(e)
    {
        e.preventDefault();

        var nombre_c = $("#nombre_c").val()
        var celular_c = $("#celular_c").val()
        var email_c = $("#email_c").val()
        var email2_c = $("#email2_c").val();
        var empresa_id = $("#empresa_id").val();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data : {
                nombre_c : nombre_c,
                celular_c : celular_c,
                email_c : email_c,
                email2_c : email2_c,
                empresa_id : empresa_id,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha registrado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
          });
    });


    $(".btnEditC").click(function(){

        var contacto_id = $(this).data("id");
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data : {
                contacto_id : contacto_id,
            },
            dataType: 'json',
            success: function(data){
                console.log(data);
                $("#editContacto").modal('show');
                $("#edit_nombre_c").val(data.nombre_c)
                $("#edit_celular_c").val(data.celular_c)
                $("#edit_email_c").val(data.email_c)
                $("#edit_email2_c").val(data.email2_c);
                $("#edit_empresa_id").val(data.empresa_id);
                $("#edit_id_contacto").val(data.id_contacto);
            }
          });
    });

    $(".frmContactoEdit").submit(function(e)
    {
        e.preventDefault();

        var nombre_c = $("#edit_nombre_c").val()
        var celular_c = $("#edit_celular_c").val()
        var email_c = $("#edit_email_c").val()
        var email2_c = $("#edit_email2_c").val();
        var empresa_id = $("#edit_empresa_id").val();
        var id_contacto = $("#edit_id_contacto").val();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data : {
                nombre_c : nombre_c,
                celular_c : celular_c,
                email_c : email_c,
                email2_c : email2_c,
                empresa_id : empresa_id,
                id_contacto : id_contacto,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha modificado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
          });
    });


    $(".btnEliminarC").click(function(){
        var id_contacto = $(this).data("id");

        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data : {
                id_contacto : id_contacto,
            },
            dataType: 'json',
            success: function(){
                swal({
                    title: "¡Bien hecho!",
                    text: "Se ha eliminado correctamente",
                    type: "success",
                    //timer: 3000
               }, 
               function(){
                    window.location.reload();
               })

            }
        });
    });

});