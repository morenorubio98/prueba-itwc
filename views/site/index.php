<?php

/* @var $this yii\web\View */

$this->title = 'Prueba ITWC';
?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
<script src="<?= Yii::$app->request->baseUrl ?>/js/script.js"></script>

<div class="site-index">

    <!--<div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>-->

    <br>
    <div class="body-content">

        <div class="row">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Empresa</a></li>
                <li><a data-toggle="tab" href="#menu1">Contacto</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="card col-12">
                        
                        <table class="main-table table table-bordered table-striped table-responsive">
                            <br>
                                <thead style="float: right;">
                                    <button type="button" data-toggle="modal" data-target="#addEmpresa" class="btn btn-info">Registrar</button>
                                </thead>
                            <br><br>
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Teléfono</th>
                                    <th scope="col">Sitio web</th>
                                    <th scope="col"><center>Editar</center></th>
                                    <th scope="col"><center>Eliminar</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($empresas as $key => $emp): ?>
                                    <tr>
                                        <td><?php echo $emp["nombre_e"]; ?></td>
                                        <td><?php echo $emp["direccion_e"]; ?></td>
                                        <td><?php echo $emp["telefono_e"]; ?></td>
                                        <td><a target="_blank" href="<?php echo $emp["sitio_e"]; ?>"><?php echo $emp["sitio_e"]; ?></a></td>
                                        <td><center><button data-url="<?php echo Yii::$app->getUrlManager()->createUrl('site/editar_empresa');?>" data-id="<?php echo $emp["id_empresa"] ?>" class="btn btn-success btnEditE"><i class="fa fa-edit"></i></button></center></td>
                                        <td><center><button  data-url="<?php echo Yii::$app->getUrlManager()->createUrl('site/eliminar_empresa');?>" data-id="<?php echo $emp["id_empresa"] ?>" class="btn btn-danger btnEliminarE"><i class="fa fa-trash"></i></button></center></td>
                                    </tr>
                                <?php endforeach; ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="card col-12">
                        <table class="main-table table table-bordered table-striped table-responsive">
                            <br>
                                <thead style="float: right;">
                                    <button type="button" data-toggle="modal" data-target="#addContacto" class="btn btn-info ">Registrar</button>
                                </thead>
                            <br><br>
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Celular</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Email 2</th>
                                        <th scope="col">Empresa</th>
                                        <th scope="col"><center>Editar</center></th>
                                        <th scope="col"><center>Eliminar</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($contactos as $key2 => $con): ?>
                                    <tr>
                                        <td><?php echo $con["nombre_c"] ?></td>
                                        <td><?php echo $con["celular_c"] ?></td>
                                        <td><?php echo $con["email_c"] ?></td>
                                        <td><?php echo $con["email2_c"] ?></td>
                                        <?php if(!empty($con["empresa_id"])): ?>
                                            <td><?php echo $con["nombre_e"] ?></td>
                                        <?php else: ?>
                                            <td><?php echo "No posee" ?></td>
                                        <?php endif; ?>
                                        <td><center><button data-url="<?php echo Yii::$app->getUrlManager()->createUrl('site/editar_contacto');?>" data-id="<?php echo $con["id_contacto"] ?>" class="btn btn-success btnEditC"><i class="fa fa-edit"></i></button></center></td>
                                        <td><center><button  data-url="<?php echo Yii::$app->getUrlManager()->createUrl('site/eliminar_contacto');?>" data-id="<?php echo $con["id_contacto"] ?>" class="btn btn-danger btnEliminarC"><i class="fa fa-trash"></i></button></center></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<!-- Modal agregar Empresa-->
<div class="modal fade" id="addEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Registrar Empresa</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="frmEmpresa" action="<?php echo Yii::$app->getUrlManager()->createUrl('site/guardar_empresa');?>" method="POST">
            <div class="form-group">
                <label for="nombre_e">Nombre</label>
                <input type="text" class="form-control" name="nombre_e" id="nombre_e" placeholder="IT Web Consultants" required>
            </div>
            <div class="form-group">
                <label for="direccion_e">Dirección</label>
                <input type="text" class="form-control" name="direccion_e" id="direccion_e" placeholder="San Salvador">
            </div>
            <div class="form-group">
                <label for="telefono_e">Teléfono</label>
                <input type="text" class="form-control" name="telefono_e" id="telefono_e" placeholder="0000-0000" required>
            </div>
            <div class="form-group">
                <label for="sitio_e">Sitio web</label>
                <input type="text" class="form-control" name="sitio_e" id="sitio_e" placeholder="https://it-webconsultants.com/Our-offices/">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- fin modal agregar empresa -->

<!-- Modal editar Empresa-->
<div class="modal fade" id="editEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Editar Empresa</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="frmEmpresaEdit" action="<?php echo Yii::$app->getUrlManager()->createUrl('site/modificar_empresa');?>" method="POST">
            <div class="form-group">
                <label for="nombre_e">Nombre</label>
                <input type="text" class="form-control" name="nombre_e" id="edit_nombre_e" placeholder="IT Web Consultants" required>
            </div>
            <div class="form-group">
                <label for="direccion_e">Dirección</label>
                <input type="text" class="form-control" name="direccion_e" id="edit_direccion_e" placeholder="San Salvador">
            </div>
            <div class="form-group">
                <label for="telefono_e">Teléfono</label>
                <input type="text" class="form-control" name="telefono_e" id="edit_telefono_e" placeholder="0000-0000" required>
            </div>
            <div class="form-group">
                <label for="sitio_e">Sitio web</label>
                <input type="text" class="form-control" name="sitio_e" id="edit_sitio_e" placeholder="https://it-webconsultants.com/Our-offices/">
            </div>
            <input type="hidden" name="id_empresa" id="edit_id_empresa">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- fin modal agregar empresa -->

<!-- Modal agregar Contacto-->
<div class="modal fade" id="addContacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Registrar Contacto</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="frmContacto" action="<?php echo Yii::$app->getUrlManager()->createUrl('site/guardar_contacto');?>" method="POST">
            <div class="form-group">
                <label for="nombre_c">Nombre</label>
                <input type="text" class="form-control" name="nombre_c" id="nombre_c" placeholder="Alejandro Moreno" required>
            </div>
            <div class="form-group">
                <label for="celular_c">Celular</label>
                <input type="text" class="form-control" name="celular_c" id="celular_c" placeholder="0000-0000" required>
            </div>
            <div class="form-group">
                <label for="email_c">Email</label>
                <input type="email" class="form-control" name="email_c" id="email_c" placeholder="example@gmail.com" required>
            </div>
            <div class="form-group">
                <label for="email2_c">Email 2</label>
                <input type="email" class="form-control" name="email2_c" id="email2_c" placeholder="example@gmail.com">
            </div>
            <div class="form-group">
                <label for="empresa_id">Empresa</label>
                <select class="form-control select2" id="empresa_id" name="empresa_id">
                    <option value="">Seleccionar empresa</option>
                    <option value="">No posee</option>
                    <?php foreach($empresas as $key3 => $emp_m): ?>
                        <option value="<?php echo $emp_m['id_empresa'] ?>"><?php echo $emp_m['nombre_e']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>



<!-- Modal editar Contacto-->
<div class="modal fade" id="editContacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Editar Contacto</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="frmContactoEdit" action="<?php echo Yii::$app->getUrlManager()->createUrl('site/modificar_contacto');?>" method="POST">
        <div class="form-group">
                <label for="nombre_c">Nombre</label>
                <input type="text" class="form-control" name="nombre_c" id="edit_nombre_c" placeholder="Alejandro Moreno" required>
            </div>
            <div class="form-group">
                <label for="celular_c">Celular</label>
                <input type="text" class="form-control" name="celular_c" id="edit_celular_c" placeholder="0000-0000" required>
            </div>
            <div class="form-group">
                <label for="email_c">Email</label>
                <input type="email" class="form-control" name="email_c" id="edit_email_c" placeholder="example@gmail.com" required>
            </div>
            <div class="form-group">
                <label for="email2_c">Email 2</label>
                <input type="email" class="form-control" name="email2_c" id="edit_email2_c" placeholder="example@gmail.com">
            </div>
            <div class="form-group">
                <label for="empresa_id">Empresa</label>
                <select class="form-control select2" id="edit_empresa_id" name="empresa_id">
                    <option value="">Seleccionar empresa</option>
                    <option value="">No posee</option>
                    <?php foreach($empresas as $key3 => $emp_m): ?>
                        <option value="<?php echo $emp_m['id_empresa'] ?>"><?php echo $emp_m['nombre_e']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="hidden" name="id_contacto" id="edit_id_contacto">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>