<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "contactos".
 *
 * @property int $id_contacto
 * @property string $nombre_c
 * @property string $celular_c
 * @property string $email_c
 * @property string|null $email2_c
 * @property int|null $empresa_id
 * @property int $activo
 * @property string $creado
 */
class Contactos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contactos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_c', 'celular_c', 'email_c'], 'required'],
            [['email_c', 'email2_c'], 'string'],
            [['empresa_id', 'activo'], 'integer'],
            [['creado'], 'safe'],
            [['nombre_c'], 'string', 'max' => 100],
            [['celular_c'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contacto' => 'Id Contacto',
            'nombre_c' => 'Nombre C',
            'celular_c' => 'Celular C',
            'email_c' => 'Email C',
            'email2_c' => 'Email2 C',
            'empresa_id' => 'Empresa ID',
            'activo' => 'Activo',
            'creado' => 'Creado',
        ];
    }

    public static function saveContacto($array){
        $cn = Yii::$app->db;
        $cn->createCommand()->insert("contactos",$array)->execute();

        return $cn->getLastInsertID();
    }

    public static function getAllContactos($nombre = null)
    {
        $query = new Query();
        $query->select(["*"])
            ->from('contactos c')
            ->where('c.activo = 1')
            ->join('LEFT JOIN', 'empresas e', 'e.id_empresa = c.empresa_id');

        return $query;
    }

    public static function getContacto($contacto_id)
    {
        $query = new Query();
        $query->select(["*"])
            ->from('contactos')
            ->where('activo = 1')
            ->andWhere("id_contacto = '$contacto_id' ");

        return $query;
    }

    public static function updateContacto($data=[]){
        $cn = Yii::$app->db;
        return $cn->createCommand()->update("contactos",$data," id_contacto =". $data["id_contacto"])->execute();
    }

    public static function eliminarContacto($arreglo=[],$data){
        $cn = Yii::$app->db;
        return $cn->createCommand()->update("contactos",$arreglo," id_contacto =". $data)->execute();
    }
}
