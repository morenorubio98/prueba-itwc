<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "empresas".
 *
 * @property int $id_empresa
 * @property string $nombre_e
 * @property string|null $direccion_e
 * @property string $telefono_e
 * @property string|null $sitio_e
 * @property int $activo
 * @property string $creado
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_e', 'telefono_e'], 'required'],
            [['sitio_e'], 'string'],
            [['activo'], 'integer'],
            [['creado'], 'safe'],
            [['nombre_e', 'direccion_e'], 'string', 'max' => 100],
            [['telefono_e'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empresa' => 'Id Empresa',
            'nombre_e' => 'Nombre E',
            'direccion_e' => 'Direccion E',
            'telefono_e' => 'Telefono E',
            'sitio_e' => 'Sitio E',
            'activo' => 'Activo',
            'creado' => 'Creado',
        ];
    }

    public static function saveEmpresa($array){
        $cn = Yii::$app->db;
        $cn->createCommand()->insert("empresas",$array)->execute();

        return $cn->getLastInsertID();
    }

    public static function getAllEmpresas()
    {
        $query = new Query();
        $query->select(["*"])
            ->from('empresas')
            ->where('activo = 1');

        return $query;
    }

    public static function getEmpresa($empresa_id)
    {
        $query = new Query();
        $query->select(["*"])
            ->from('empresas')
            ->where('activo = 1')
            ->andWhere("id_empresa = '$empresa_id' ");

        return $query;
    }

    public static function updateEmpresa($data=[]){
        $cn = Yii::$app->db;
        return $cn->createCommand()->update("empresas",$data," id_empresa =". $data["id_empresa"])->execute();
    }

    public static function eliminarEmpresa($arreglo=[],$data){
        $cn = Yii::$app->db;
        return $cn->createCommand()->update("empresas",$arreglo," id_empresa =". $data)->execute();
    }
}
